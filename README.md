# Vasille Language

Vasille Language for Vasille.js Framework

## Transactions

### Root context
* `component` -> `scriptTag`
* `component` -> `scriptTag` `vhtmlTag`
* `component` -> `scriptTag` `vhtmlTag` `styleTag`

* `scriptTag` -> `<script>` `vjsCode` `</script>`
* `vhtmlTag` -> `<App>` `htmlTags` `</App>`
* `styleTag` -> `<style>` `vStyle` `</style>`

### VJS Context
* `vjsCode` -> `declaration` `;` `vjsCode`
* `vjsCode` -> `declaration` `vjsCode`
* `declaration` -> `dataD` | `propD` | `exprD` | `methodD` | `eventD` | `watcherD` | `importD` | `pointerD`

* `dataD` -> `let` `variableD`
* `propD` -> `export` `dataD`
* `exprD` -> `dataD`
* `pointerD` -> `var` `variableD`
* `variableD` -> `maybeTypedVar`
* `variableD` -> `maybeTypedVar` `,` `variableD`
* `maybeTypedVar` -> `varName` `type`
* `maybeTypedVar` -> `varName` `assign`
* `maybeTypedVar` -> `varName` `type` `assign`
* `varName` -> `jsIdentifier`
* `typeName` -> `jsIdentifier`
* `type` -> `:` `typeName`
* `assign` -> `=` `valuableExpression`

* `valuableExpression` -> `jsNumber` | `jsString` | `jsArray` | `jsObject` | `jsIdentiier` | `functionCall`
* `functionCall` -> `functionName` `(` `args` `)`
* `functionCall` -> `new` `functionName` `(` `args` `)`
* `functionName` -> `jsIdentitier`
* `args` -> `valuableExpression` `,` `args`

* `methodD` -> `function` `functionName` `(` `params` `)` `{` `jsCode` `}`
* `methodD` -> `function` `functionName` `(` `params` `)` `type` `{` `jsCode` `}`
* `eventD` -> `declare` `function` `functionName` `(` `params` `)`
* `watcherD` -> `let` `watcherName` `=` `function` `(` `)` `{` `jsCode` `}` `.` `call` `(` `)`
* `params` -> `param` `,` `params`
* `param` -> `paramName` `type`
* `param` -> `paramName`

### JS Context (Strong as fragments)
* `jsCode` -> `anyExpr` `;` `jsCode`
* `jsCode` -> `anyExpr` `jsCode`
* to continue after

### HTML Context
* `htmlTags` -> `htmlTag` `htmlTags`
* `htmlTag` -> `<` `name` `attrs` `>` `htmlTags` `<` `/` `name` `>`
* `attr` -> `name` `=` `"` string value `"`
* `attr` -> `name` `=` `'` `jsCode` `'`
* `attr` -> `:` `name` `=` `"` `jsCode` `"`
* `attr` -> `name` `.` `modifier` `=` value

### Style Context
* `vStyle` -> `selector` `{` `rules` `}`
* `vStyle` -> `mediaRule` `{` `vStyle` `}`
* `vStyle` -> `$global` `{` `vStyle` `}`
* `selector` -> `sPart` `selector`
* `selector` -> `sPart` `|` `selector`
* `selector` -> `sPart` `operator` `selector`
* `selector` -> `sPart`
* `sPart` -> `tagName` | `className` | `id` | `suffix`
* `suffix` -> `:` `suffixName`
* `suffix` -> `::` `suffixName`
* `suffix` -> `:` `suffixName` `(` `sPart` `)`
* `mediaRules` -> `@` `ruleName` `mediaArgs`
* `mediaArgs` -> `mediaArg` `mediaArgs`
* `mediaArg` -> `identiier`
* `mediaArg` -> `(` `rule` `)`
* `rules` -> `rule` `;` `rules`
* `rule` -> `propName` `:` `propValue`
